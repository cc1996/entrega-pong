﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour {
	
	public float vely;
	private Vector3 initPos;
	private float move;
	private Vector3 actualPosition;
	public float maxy;
	private Vector3 tmpPosition;

	void Awake(){
		initPos = transform.position;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		move = Input.GetAxis ("Vertical");

		transform.Translate (0, move * vely * Time.deltaTime, 0);

		if (transform.position.y > maxy) {
			tmpPosition = new Vector3 (transform.position.x, maxy, transform.position.z);
			transform.position = tmpPosition;
		}else if (transform.position.y < -maxy){
			tmpPosition = new Vector3 (transform.position.x, -maxy, transform.position.z);
			transform.position = tmpPosition;
		 }
}
}
