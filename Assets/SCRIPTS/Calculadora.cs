﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Calculadora : MonoBehaviour {
	public string nombre;
	public static int suma(int a, int b){
		Debug.Log ("Suma Entera");
		return a + b;
	}
	public int resta(int a, int b){
		return a - b;
	}
	public int multiplicacion(int a, int b){
		return a * b;
	}
	public int division(int a, int b){
		return a / b;
		}

	public static float suma(float a, float b){
		Debug.Log ("Suma float");
		return a + b;
	}
	public float resta(float a, float b){
		return a - b;
	}
	public float multiplicacion(float a, float b){
		return a * b;
	}
	public float division(float a, float b){
		return a / b;
	}


	// Use this for initialization
	void Start () {
		Debug.Log ("Hola : " + nombre);
		Debug.Log (suma(5f, 10f));

		Debug.Log (resta (10, 2));
		Debug.Log (multiplicacion (3, 4));
		Debug.Log (division (10, 2));

		int a = 5;
		int b = 4;
		int c = resta (a, b);
		Debug.Log ("La resta de a :" + a + "y b" + b + "es :" + c); 
		
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
