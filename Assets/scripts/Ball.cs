﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	private Vector3 initPos;
	public Vector2 movement;
	public float maxy;
	public float maxx;

	// Use this for initialization
	void Awake () {

		initPos = transform.position;
		}
	
	// Update is called once per frame
	void Update () {
		transform.Translate (movement.x * Time.deltaTime, movement.y * Time.deltaTime, 0);
	}

	void OnTriggerEnter(Collider other){
		switch (other.tag) {
		case "Player":
			movement.x *= -1.25f;
			break;
		case "goal":
			movement.x *= -1.25f;
			break;
		case "border":
			movement.y *= -1.25f;
			break;


		}
		if (movement.x > 15f) {
			movement.x = 15f;
		}else if (movement.x < -15f){
			movement.x = -15f;
		}
		if (movement.y > 15f) {
			movement.y = 15f;
		}else if (movement.y < -15f){
			movement.y = -15f;
		}

	}
}
